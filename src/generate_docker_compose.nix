{ pkgs ? import <nixpkgs> {}, composition ? { }, extraConfigurations ? [ ],  ... }:

let
  # name and tag of the base container image
  name = "nxc-docker-base-image";
  tag = "latest";
  dockerComposeConfig = {
    version = "3.4";
    x-nxc.image = import ./generate_image.nix {inherit pkgs name tag; };
  };
  baseEnv = pkgs.buildEnv { name = "container-system-env"; paths = [ pkgs.bashInteractive pkgs.coreutils ]; };

  dockerComposeConfig.services = builtins.mapAttrs (nodeName: nodeConfig:
    let
      config = {
        imports = [
          ./systemd.nix
          nodeConfig
        ] ++ extraConfigurations;
      };
      builtConfig = pkgs.nixos config;
    in
    {
      cap_add = [ "SYS_ADMIN" ];
      command = [ "${builtConfig.toplevel}/init" ];
      environment = {
        NIX_REMOTE = "";
        PATH = "/bin:/usr/bin:/run/current-system/sw/bin";
        container = "docker";
      };
      hostname = nodeName;
      image = "${name}:${tag}";
      stop_signal = "SIGINT";
      tmpfs = [
        "/run"
        "/run/wrappers:exec,suid"
        "/tmp:exec,mode=777"
      ];
      tty = true;
      volumes = [
        "/sys/fs/cgroup:/sys/fs/cgroup:ro"
        "/nix/store:/nix/store:ro"
        "${baseEnv}:/run/system:ro"
      ];
  }) composition.nodes;

in
  dockerComposeConfig

