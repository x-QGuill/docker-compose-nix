{ config, pkgs, ... }:

{
  config = {
    environment.systemPackages = [
      pkgs.hello
    ];
  };
}
