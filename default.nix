{ pkgs ? import <nixpkgs> {}, ... }:

let
  composition = import ./composition.nix { inherit pkgs; };
  dockerComposeConfig = import ./src/generate_docker_compose.nix { inherit pkgs composition; };
in
  pkgs.writeTextFile { name = "dc.json"; text = (builtins.toJSON dockerComposeConfig); }
