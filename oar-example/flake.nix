{
  description = "nixos-compose - composition to infrastructure";

  inputs.NUR.url = "github:nix-community/NUR";
  inputs.kapack.url = "github:oar-team/nur-kapack?rev=4affc017172a69cc1f0ab2758c93195c403de9e8";
  outputs = { self, nixpkgs, NUR, kapack }:
    let
      system = "x86_64-linux";

      nur = import ./nix/nur.nix {
        inherit nixpkgs system NUR;
        repoOverrides = { inherit kapack; };
      };

      extraConfigurations = [
        # add nur attribute to pkgs
        { nixpkgs.overlays = [ nur.overlay ]; }
        nur.repos.kapack.modules.oar
      ];

    in {
      # packages.${system} = nixpkgs.lib.mapAttrs (name: flavour:
      #   (import ./nix/compose.nix) {
      #     inherit nixpkgs system extraConfigurations flavour;
      #   }) flavours;
      packages.${system}.dc =
        let
          pkgs = import nixpkgs { inherit system; };
          modulesPath = "${toString nixpkgs}/nixos";
          composition = import ./composition.nix { inherit pkgs modulesPath; };
          dockerComposeConfig = import ./../src/generate_docker_compose.nix { inherit pkgs composition extraConfigurations; };
        in
          pkgs.writeTextFile { name = "dc.json"; text = (builtins.toJSON dockerComposeConfig); };

      defaultPackage.${system} = self.packages.${system}.dc;

    };
}
